from setuptools import setup

setup(
    name='fueled-api-doc',
    version='0.0.1',
    packages=['api_doc', 'api_doc.templatetags'],
    zip_safe=False,
    include_package_data=True,
    url='http://github.com/Fueled/fueled-api-doc',
    license='BSD',
    author='pej-fueled',
    author_email='pej@fueled.com',
    description='Documentation for fueled APIs',
    install_requires=['markdown', 'django-api-playground>=0.1.0'],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Web Environment",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        'Programming Language :: Python :: 2.7',
        "Framework :: Django",
    ],
)

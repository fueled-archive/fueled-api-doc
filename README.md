Fueled API Doc
=====================

## Disclaimer
This repo is public at the moment as this makes it easier to install it on local machines and on staging servers. This might change soon.

## Intro
This small App extends Django-api-playground with our special needs.

## Installation

In order to use it here are the following steps to follow :

* Install plugins with pip
* Add them to INSTALLED_APPS
* Specify some settings (tastypie instance location)
* Create a basic doc file
* Add the urls

### Install plugins with pip

Basically, you need to install `Django-API-playground` which contains all the basic features, and `fueled-api-doc` which contains some specific features such as reading tastypie schema to create doc automatically


install django apli playground
Note : When it'll be published on PyPI, installing fueled-api-doc will be enough, and this step won't be necessary

```bash
pip install git+git://github.com/Hipo/Django-API-Playground.git@9dbc15221adf7295af3eb101da695740903e7072
```


And finally install this app :

```bash
pip install git+git://github.com/Fueled/fueled-api-doc.git
```
Note : It might be a good idea to specify a commit or a branch with `@commit-sha or @branch` at the end.

### Add them to INSTALLED_APPS

After that, add the app and `django api playground` to the `INSTALLED_APPS` :

```python
INSTALLED_APPS = (
	...
    'apiplayground',
    'api_doc',
)
```

### Specify some settings (tastypie instance location)

In `settings.py`, point it to your instance of tasypie like that :

```python
API_LIB_PATH = 'core.urls.v1_api'
API_PLAYGROUND_FEEDBACK = False # this is only optional, but recommended
```

### Create a basic doc file

and finally create a basic description of the api, by default name the file `playground.py` (filename can be whatever you want) :

```python
from api_doc.doc import APIDoc
from django.http import HttpRequest


class CooliAPIDoc(APIDoc):

    schema = {
        "title": "Cooli API Documentation",
        "resources": []
    }
```


### Add the urls

In a url file (will probably be `core.urls.py`)

```python
...
from .playground import CooliAPIDoc
...

urlpatterns += patterns('core.views',
    url(r'api-doc/', include(CooliAPIDoc().urls)),
)

...

```

### Finally

Once this is done you already have a basic documentation for your API.


## Customization

### Common part :

The documentation will include a big block of common documentation at the beginning of the page. This documentation should be common to most repos, such as :

* User creation
* Facebook login
* Reset password
* etc …

If you want to completety remove this file or this part of the doc, create the following template : `templates/api_doc/common.md`

The content of the file should be markdown (as it's usually easier to write than HTML)

#### Update common part

If the basic documentation needs updates, submit a pull request to this repo, or edit it directly. The file to edit is `common.md`

#### Append specific part to the common part

If it needs specific addition, you can add some by creating a template with the correct name : `templates/api_doc/extra.html` (It'll be possible to write this file in markdown soon)

The content of this template will be inserted after the common doc and before the endpoints doc.

### Resources/Endpoints


You can customize the endpoint documentation by extending the `playgrounds.py` file :

Basically it's a list of `resources`, each resource is a dict which accepts the following keys :

```python
{
  'name': 'Resource name',
  'description': 'A short description of the resource',
  'endpoints': []  # A list of endpoints
}
```

Each endpoint is a dict wich accepts the following keys, note that to override generated endpoint, it has to match both the `method` and the `url` key

```python
{
  'method': 'HTTP method of the endpoint',
  'url': 'url of the endpoint (including /api/v1/ ...)',
  'description': 'A short description of the endpoint',
  'full_description': 'It can be markdown, in order to include rich cotnent (<li>, <code> etc ...)'
}
```

Here is an example :

```python
class ExampleAPIDoc(APIDoc):

    schema = {
        "title": "API Documentation",
        "base_url": "/api/v1/",
        "resources": [
            {
                "name": "create_user",
                "description": "Create a user for the application. This is not needed if using Facebook authentication. (This can also be markdown)",
                "endpoints": [
                    {
                        "method": "POST",
                        "url": "/api/v1/create_user/",
                        "description": "Creates new user. The response contains a key that will need to be used for any authenticated calls.",
                        "full_description": """
A user can be created with the following body via a POST request.

    {
        "username": "murphy@ocp.gov",
        "password": "pass",
        "first_name": "Alex",
        "last_name": "Murphy",
        "apple_device_token": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    }

                        """,
                        "parameters": [
                            {
                                "name": "password",
                                "type": "string"
                            },
                            {
                                "name": "apple_device_token",
                                "type": "string"
                            },
                        ]
                    }
                ]
            },
            {
                "name": "another resource",
                "description": "TODO",
                "endpoints": [
                    {
                        "method": "GET",
                        "url": "/fb_login/?access_token={access_token}&apple_device_token={apple_device_token}/",
                        "description": "TODO",
                    },
                ]
            },
         }
     }
```


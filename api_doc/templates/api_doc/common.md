## Authentication

### Authentication Steps

1. For any authentication method that is used, an ApiKey will be returned. See the section making authenticated calls for more info.
2. There are multiple ways to authenticate a user (i.e., obtain an ApiKey):
    - Create a user.
    - Obtain a user's info from the Facebook login endpoint.
    - Get an existing user's info from the user profile endpoint.

###Creating a User with Username/Password

Watch out! You do not need to create a user when a user logs in via Facebook. Use the Facebook login endpoint for that.

To create a user, use the create user endpoint.

### Making Authenticated Calls
You'll only need to use the key for some of the endpoints. Any time it's required you'll need to pass it in the HTTP header using the following key/value pair.

    key: Authorization
    value: ApiKey username:key

For example: <code>Authorization: ApiKey murphy@ocp.gov:204db7bcfafb2deb7406b89eb3b9b715b09905c8</code>

### Password Reset for iOS

1. A password can be reset from the iOS app by sending a request to <code>/email_password_reset/?username=murphy@ocp.gov</code>, of course by replacing that request with the correct username. This is a plain HTTP GET request. Nothing special needs to be passed into the header.
2. The user is then sent an email with a link that must be clicked from their phone. A link will look like <code>segg://token=murphy@ocp.gov:1Ty9Cn:Oi9ALrxwQUtp31JTPackTfRto_I</code>.
3. The app should then do an HTTP POST request to <code>/reset_password/</code> with the <code>token</code> and the new <code>password</code> for the user (note, this is not passed in the body as JSON, just POST the form key/value pairs). Once this is done, the password will be reset and the user should be taken to the login screen. An email confirmation is sent to the user.

## Using the API

### Schemas
Each endpoint has an associated schema that defines required/optional fields, the HTTP Methods allowed (e.g., POST, GET), the field data types, help info, etc.

To view a schema for an endpoint, just add <code>/schema</code> to the end of the URL. For example: <code>GET /api/v1/occasion/schema?format=json</code>.

Watch out! If an endpoint requires authentication, then the schema for the endpoint also requires authentication. It is recommended to use a REST client like POSTMAN to view schemas.

### Pagination

- By default, you get a paginated set of objects.
- In the response, you get pagination data under the <code>meta</code> key.
- In the meta, you get a <code>previous</code> & <code>next</code>. If available, these are URIs to the previous & next pages.
- You get a list of objects under the <code>objects</code> key.
- Each object has a <code>resource_uri</code> field that points to the detail view for that object.
- To control the list size you can set the GET URL params <code>limit</code> and <code>offset</code>.
- Passing in <code>limit=0</code> will skip pagination. Be careful using this for large amounts of data!

### Sample Meta

    "meta": {
        "limit": 20,
        "next": null,
        "offset": 0,
        "previous": null,
        "total_count": 8
    }

### Selecting a Subset

You can cherry pick a subset of lists by adding <code>/set</code> to an endpoint and passing in the IDs of the resources separated by a semicolon. For example, <code>GET /api/v1/product/set/1;3;4?format=json</code>.

Watch out! The last ID on the list does not have a semicolon at the end. Be sure to leave it off.

In this example, 3 and 4 were not found:

    {
        "not_found": [
            "3",
            "4"
        ],
        "objects": [
            {
                "for_females": true,
                "for_males": true,
                "full_description": "You want this robot!",
                "id": 1,
                "images": [
                    {
                        "file": "/media/img/product/large/2013/01/product_1.jpg",
                        "id": 1,
                        "position": 0,
                        "resource_uri": "/api/v1/product_image/1/"
                    }
                ],
                "is_active": true,
                "is_gift_idea": true,
                "max_age": 97,
                "min_age": 1,
                "price": "3.50",
                "resource_uri": "/api/v1/product/1/",
                "short_description": "The greatest gift ever invented.",
                "title": "RoboCop"
            }
        ]
    }

### Ordering and Filtering

Need to order or filter lists from an endpoint in a special way? No problem! Just make a request to you know who and it can be added to any endpoint.

### Errors

Conventional HTTP response codes are used to indicate success or failure of an API request. In general:

- codes in the 2xx range indicate success.
- codes in the 4xx range indicate an error that resulted from the provided information (e.g., a date was not valid).
- codes in the 5xx range indicate an error with the server.

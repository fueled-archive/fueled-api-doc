import json
import markdown
import sys

from django.conf.urls import url
from django.conf import settings
from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import render_to_response
from django.template import RequestContext, TemplateDoesNotExist, Context
from django.template.loader import get_template
from django.db.utils import DatabaseError

from apiplayground import APIPlayground


class ImproperlyConfigured(Exception):
    pass


class APIDoc(APIPlayground):

    index_template = "api_doc/index.html"

    def get_urls(self):
        """
        Returns API Browser URLs.
        You can override method for adding extra views.
        """
        return [
            url("^$", self.api_index, name="api_doc_index"),
        ]

    def api_index(self, request):
        """
        A view that returns _api browser index.
        """
        self.build_full_description()
        external_schema = self.get_external_api_schema()
        schema = self.get_schema()

        self.merge_schemas(schema, external_schema)

        markdown_template = get_template('api_doc/common.md')
        markdown_filtered_content = markdown.markdown(
            markdown_template.render(Context({})))

        if request.user.is_authenticated():
            user = request.user
            try:
                # TODO fix that in a more generic way
                # This is needed here to prevent an ImportError when running with
                # Django 1.5
                from tastypie.models import ApiKey
                api_key = ApiKey.objects.get(user_id=user.id)
            except (ApiKey.DoesNotExist, DatabaseError):
                api_key = None
        else:
            user = None
            api_key = None

        return render_to_response(self.index_template, {
            "schema": schema,
            "feedback_form": self.get_feedback_form(request),
            "markdown_filtered_content": markdown_filtered_content,
            "current_user": user,
            "api_key": api_key,
            "feedback_form_toggle": False,
        }, context_instance=RequestContext(request))

    def root_uri(self):
        """
        Returns the root URI of the API

        TODO : Find a generic way to do that
        """
        return '/api/v1'

    def merge_schemas(self, schema, external_schema):
        """
        Try to merge data from the external schema into the user defined schema
        Doesn't return anything but modify the schema parameter
        """
        resources = schema['resources']
        for external_resource in external_schema.get('resources', []):
            # find the resource to be merged in the schema
            resource_index = -1
            for i, resource in enumerate(schema['resources']):
                if resource['name'] == external_resource['name']:
                    resource_index = i
                    break

            if resource_index >= 0:
                resource = resources[resource_index]
                self.merge_endpoints(resource['endpoints'],
                                     external_resource['endpoints'])
            else:
                # The resource is only defined in the external API
                schema['resources'].append(external_resource)

    def merge_endpoints(self, endpoints, external_endpoints):
        """
        Merge end the endpoints contained in external_endpoints into endpoints
        Doesn't return anything but modify endpoints
        """
        for external_endpoint in external_endpoints:
            # find common endpoints
            endpoint_index = -1
            for i, endpoint in enumerate(endpoints):
                if ((endpoint['url'] == external_endpoint['url'] and
                        endpoint['method'] == external_endpoint['method'])):
                    endpoint_index = i

            if endpoint_index >= 0:
                endpoint = endpoints[endpoint_index]
                for external_param in external_endpoint['parameters']:
                    try:
                        endpoint['parameters'].append(external_param)
                    except KeyError:
                        endpoint['parameters'] = [external_param]
            else:
                # Endpoint is defined in the external schema only, so add it
                endpoints.append(external_endpoint)

    def build_full_description(self):
        """
        Returns the full_description transformed in html after being parsed
        by markdown
        """
        schema = self.get_schema()
        for resource in schema.get('resources', []):
            for endpoint in resource.get('endpoints', []):
                if endpoint.get('full_description', ""):
                    endpoint['full_description'] = markdown.markdown(
                        endpoint['full_description'].strip())

    def get_external_api_schema(self):
        """
        Returns the schema based on an external API engine.
        Only supports tastypie at the moment.
        """
        tastypie_api_module = getattr(settings, 'API_LIB_PATH', None)
        if not tastypie_api_module:
            raise Exception("Must define API_LIB_PATH in settings as path to a tastypie.api.Api instance")
        path, attr = tastypie_api_module.rsplit('.', 1)
        try:
            tastypie_api = getattr(sys.modules[path], attr, None)
        except KeyError:
            raise ImproperlyConfigured("%s is not a valid python path" % path)
        if not tastypie_api:
            raise ImproperlyConfigured("%s is not a valid tastypie.api.Api instance" % tastypie_api_module)
        api = tastypie_api

        tastypie_schema = {resource_name: resource.build_schema()
                           for resource_name, resource in api._registry.iteritems()}

        schema = {}
        resources = []

        for resource_name, tastypie_resource_schema in tastypie_schema.iteritems():
            resource_schema = {}
            resource_schema['name'] = resource_name
            resource_schema['description'] = "Handle different endpoints for %s" % resource_name
            resource_schema['endpoints'] = self.build_endpoint_list(resource_name, tastypie_resource_schema)

            resources.append(resource_schema)

        schema['resources'] = resources

        return schema

    def build_endpoint_list(self, resource_name, resource_schema):
        """
        Returns the endpoints list from the schema
        """
        endpoints = []

        for http_method in resource_schema.get('allowed_detail_http_methods', []):
            endpoints.append(self.build_endpoint_schema(
                'detail', http_method, resource_name, resource_schema))

        for http_method in resource_schema.get('allowed_list_http_methods', []):
            endpoints.append(self.build_endpoint_schema(
                'list', http_method, resource_name, resource_schema))

        return endpoints

    def build_endpoint_schema(self, endpoint_type, http_method, resource_name, resource_schema):
        """
        Returns the schema for the given endpoint
        """
        endpoint_schema = {}
        endpoint_schema['method'] = http_method.upper()
        endpoint_schema['description'] = "Description for %s" % resource_name
        endpoint_schema['parameters'] = self.build_endpoint_params(
            endpoint_type,
            http_method,
            resource_schema)

        if endpoint_type == 'detail':
            endpoint_schema['url'] = "%s/%s/{%s_id}/" % (self.root_uri(), resource_name, resource_name)
        else:
            endpoint_schema['url'] = "{root}/{name}/".format(root=self.root_uri(), name=resource_name)

        return endpoint_schema

    def get_field_type(self, type_name):
        """
        Returns the type of the given field so it can be used by API playground
        form.
        At the moment supported types are string and boolean
        """
        blacklist = ['datetime', 'related', 'list', 'integer', 'float']
        if type_name in blacklist:
            return 'string'
        else:
            return type_name

    def build_endpoint_params(self, endpoint_type, http_method, resource_schema):
        """
        Return the parameters list for the endpoint
        """
        params = []
        if http_method not in ['post', 'put', 'patch']:
            return params

        for field_name, field_description in resource_schema.get('fields', {}).iteritems():
            field_input_params = {
                "name": field_name,
                "type": self.get_field_type(field_description['type']),
            }
            if field_description['blank'] or field_description['nullable']:
                field_input_params['is_required'] = False
            else:
                field_input_params['is_required'] = True
            if not field_description['readonly']:
                params.append(field_input_params)

        return params

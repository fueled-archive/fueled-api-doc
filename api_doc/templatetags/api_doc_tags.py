from django import template

register = template.Library()


@register.simple_tag
def template_exists(template_name):
    try:
        loaded_template = template.loader.get_template(template_name)
        return loaded_template.render(template.Context({}))
    except template.TemplateDoesNotExist:
        return ""
